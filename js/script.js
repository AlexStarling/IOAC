const anchors = document.querySelectorAll('a[href*="#"]')
for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault()
        const blockID = anchor.getAttribute('href').substr(1)
        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}

let menuBtn = document.getElementById('menuBtn');
let menuContainer = document.getElementById('burger-menu');

let menuIconClosed = "menu-icon closed"; //class name for closed button
let menuIconOpened = "menu-icon opened"; //class name for opened button
let menuContClosed = "burger-menu closed"; //class name for closed menu
let menuContOpened = "burger-menu opened"; //class name for opened menu

menuBtn.onclick = function () {
    if (menuBtn.className === menuIconClosed) {
        menuBtn.className = menuIconOpened;
        menuContainer.className = menuContOpened;
    } else if (menuBtn.className === menuIconOpened) {
        menuBtn.className = menuIconClosed;
        menuContainer.className = menuContClosed;
    }
}